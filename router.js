const express = require('express');
const router = express.Router();
router.use("/", require("./app/main/index"));
router.use("/user", require("./app/user/user"));
module.exports = router;