const express = require('express');
const router = express.Router();
const User = require("./User");
router.use((req, res, next) => {
    var user = new User(req.body.username,req.body.email,req.body.firstname,req.body.lastname,req.body.password,req.body.repassword);
    if(user.validate()){
        next();
    } else {
        res.status(400).json({errors: user.errors});
    }
});
module.exports = router;