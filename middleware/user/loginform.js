const express = require('express');
const router = express.Router();
router.use((req, res, next) => {
    if(req.body.login && req.body.password){
        next();
    } else {
        res.status(400).json({message: "Enter username/email and password."});
    }
});
module.exports = router;