function User(username, email, firstname, lastname, password, repassword){
    this.username = username;
    this.email = email;
    this.firstname = firstname;
    this.lastname = lastname;
    this.password = password;
    this.repassword = repassword;
    this.errors = [];
    this.adderror = function(error){
        this.errors.push(error);
    }
    this.formfilled = function(){
        if(this.username && this.email && this.firstname && this.lastname){
            return true;
        } else {
            this.adderror("Fill all fields.");
            return false;
        }
    }
    this.emailvalidate = function(){
        var emailregex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var valid = emailregex.test(this.email);
        if(valid){
            return true;
        } else {
            this.adderror("Email not valid.");
            return false;
        }
    }
    this.usernamevalidate = function(){
        var usernameregex = /^[^0-9](?=.{3,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$/;
        var valid = usernameregex.test(this.username);
        if(valid){
            return true;
        } else {
            this.adderror("Username not valid.");
            return false;
        }
    }
    this.passwordvalidate = function(){
        var passwordregex = /^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})/;
        var valid = passwordregex.test(this.password);
        if(valid){
            return true;
        } else {
            this.adderror("Password not valid.");
            return false;
        }
    }
    this.repasswordvalidate = function(){
        if(this.password == this.repassword){
            return true;
        } else {
            this.adderror("Password and repassword not equal.");
            return false;
        }
    }
    this.validate = function(){
        if(this.formfilled() && this.emailvalidate() && this.usernamevalidate() && this.passwordvalidate() && this.repasswordvalidate()){
            return true;
        }
        return false;
    }
}
module.exports = User;