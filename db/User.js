var mongoose = require('mongoose');
var UserSchema = new mongoose.Schema({
    email: { type: String, unique: true, lowercase: true},
    username: { type: String, unique: true, lowercase: true },
    firstname: String,
    lastname: String,
    password: String,
    blocked: { type: String, default: false },
});
var User = mongoose.model('User', UserSchema);
module.exports = User;