const express = require('express');
const router = express.Router();
router.get('/', (req, res) => {
  res.json({message: "Xeddit API is up."})
});
module.exports = router;