const express = require('express');
const router = express.Router();
router.post('/',require("./../../middleware/user/loginform"));
router.post('/', async function (req,res) {
    var User = require('./../../db/User');
    var login = req.body.login;
    var crypto = require('crypto');
    var password = crypto.createHash('md5').update(process.env.SALT+req.body.password).digest("hex");
    var correct = await User.findOne({ $or:[{email: login, password},{username: login, password}]});
    if (correct) {
        var Login = require('./../../db/Login');
        var login = new Login({User: correct._id});
        var logged = await login.save();
        res.json({message: "Welcome!", token: logged._id});
    } else {
        res.status(400).json({message: "Email/Username or Password wrong."});
    }
});
module.exports = router;