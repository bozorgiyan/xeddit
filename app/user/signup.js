const express = require('express');
const router = express.Router();
router.post('/',require("./../../middleware/user/signupform"));
router.post('/', async function (req,res) {
    var User = require('./../../db/User');
    var busy = await User.findOne({ $or:[{email: req.body.email},{username: req.body.username}]});
    if (!busy) {
        var crypto = require('crypto');
        var passwordhash = crypto.createHash('md5').update(process.env.SALT+req.body.password).digest("hex");
        var user = new User({email: req.body.email, username: req.body.username, firstname: req.body.firstname, lastname: req.body.lastname, password: passwordhash});
        user.save((err) => {
            if (err) {
                res.status(500).json({message: "Something wrong."});
            } else {
                res.json({message: "Welcome!"});
            }
        });
    } else {
        res.status(400).json({message: "Email or Username busy."});
    }
});
module.exports = router;