// Setup env
const dotenv = require('dotenv');
dotenv.config();

const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const multer = require('multer');
var upload = multer();
app.use(bodyParser.json()); 
app.use(bodyParser.urlencoded({ extended: true }));
app.use(upload.array()); 
app.use(express.static('public'));
app.use(cors());

// Use router
app.use(require("./router"));

// Allow origin
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
// Connect mongo
const mongoose = require('mongoose');
mongoose.connect(process.env.MONGOURL, {useNewUrlParser: true, useUnifiedTopology: true});
// Start server
app.listen(process.env.PORT);